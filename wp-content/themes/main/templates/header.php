<header class="banner" role="banner">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
    <nav role="navigation">
      <?php
		if (has_nav_menu('primary_navigation')) :
		  wp_nav_menu(array(
			'theme_location' => 'primary_navigation',
			'depth'             => 2,
			'container'         => 'div',
			'container_id'      => 'top-nav',
			'container_class'   => 'collapse navbar-collapse',
			'menu_class' => 'nav navbar-nav',
			'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
			'walker'     => new wp_bootstrap_navwalker()
			));
		endif;
		?>
    </nav>
  </div>
</header>
