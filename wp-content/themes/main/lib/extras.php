<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;
use WP_Query;
/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Chống spam link comment
 */
remove_filter('comment_text', 'make_clickable', 9);

/**
 * Gỡ bỏ các thông số kích thước ảnh
 */
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
add_filter('post_thumbnail_html', __NAMESPACE__ . '\\remove_width_attribute');
add_filter('image_send_to_editor', __NAMESPACE__ . '\\remove_width_attribute');
/**
 * Giữ chất lượng ảnh
 **/
add_filter('jpeg_quality', 'jpeg_quality_callback');
function jpeg_quality_callback($arg) {
   return (int)100;
}