# Wordpress Boilerplate #
Bộ công cụ lập trình Wordpress tích hợp sẵn các thành phần cần thiết do Techcube Media JSC tổng hợp.

## Các thông số cài đặt chung ##
Username: **admin**

Password: **123456**


## Sử dụng ##

Cài đặt wordpress như bình thường.

1. Cài đặt Wordpress.
2. Kích hoạt các plugin cần thiết (plugin **Soil** là bắt buộc)
3. Copy các thành phần assets cần thiết từ việc build Frontend vào thư mục **wp-content/themes/main/dist/**
4. Ráp giao diện vào các file tương ứng

## Cấu hình giao diện (wp-content/theme/main/lib/) ##
- **config.php**: Cấu hình vị trí hiển thị sidebar
- **extras.php**: Viết các Shortcode / Function / Filter
- **init.php**: Khai báo Navigation / Sidebar

## Cấu trúc template ##
### Các thành phần chính (wp-content/theme/main/) ###
- **404.php**: trang lỗi 404
- **base.php**: trang khung
- **index.php**: các trang danh sách (archive) bao gồm trang danh sách bài viết, danh sách category, danh sách tác giả...)
- **page.php**: trang đơn
- **single.php**: trang chi tiết bài viết
- **template-custom.php**: template tùy chọn (thường dùng làm layout cho trang liên hệ)
### Các thành phần chi tiết (wp-content/theme/main/templates/) ###
- **comments.php**: Item comment
- **content-page.php**: Nội dung trang đơn (*include bởi `page.php`*)
- **content-single.php**: Nội dung chi tiết bài viết (*include bởi `single.php`*)
- **content.php**: Item trang danh sách (*include bởi `index.php`*)
- **entry-meta.php**: Phần ngày tháng, tác giả (*include bởi `content-single.php`*)
- **footer.php**: Footer (*include bởi `base.php`*)
- **header.php**: Header (*include bởi `base.php`*)
- **head.php**: Header meta (*include bởi `base.php`*)
- **page-header.php**: Tiêu đề trang đơn
- **searchform.php**: Khung search
- **sidebar.php**: Sidebar (*include bởi `base.php`*)

### Mở rộng ###
Áp dụng tính kế thừa của giao diện wordpress [Tham khảo](https://developer.wordpress.org/themes/basics/template-hierarchy/)

Có thể mở rộng template bằng cách tạo thêm tập tin dựa vào các file có sẵn.
Ví dụ: Copy file **page.php** thành trang **page-thoi-su.php** rồi chỉnh sửa, nó sẽ chỉ áp dụng cho trang đơn có slug là `thoi-su`. 

![](https://developer.wordpress.org/files/2014/10/wp-template-hierarchy.jpg)
